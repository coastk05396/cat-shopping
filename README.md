# Deno Cat 

## 使用技術

- [Deno](https://deno.land/)
- [TypeScript](https://www.typescriptlang.org/)
- [Fresh](https://fresh.deno.dev/)

## 啟動

- 安裝 `deno`
- 在本機執行 `deno task start` 啟動測試環境
- 執行 `deno test` 可執行測試
- 打開瀏覽器 `http://localhost:8000/`

## CICD

- 執行測試
    - Artifacts測試報告下載
- 打包成Docker Image
    - 使用雲端自架的GitLab Runner
- 上傳至GItLab Registery
    - 版本控制 每次更新 使用Tag latest版本
- Deploy自動化到三大雲端(Docker Compose)
    - Digital Ocean 
    - AWS
    - GCP

![CICD](/Demo/CICD.png)

photo credits: [unsplash](https://unsplash.com/)


